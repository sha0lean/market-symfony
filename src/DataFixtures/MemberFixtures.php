<?php

namespace App\DataFixtures;

use App\Entity\Member;
use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Repository\FamilyRepository;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

//! "DependentFixtureInterface" sert a créer un ordre logique entre le chargement des fixtures selon leurs dépendances 
class MemberFixtures extends Fixture implements DependentFixtureInterface
{
    //? on instancie le FamilyRepository dans un tableau 
    //? pour aller les chercher dans la base de données  
    private $familyRepository;

    public function __construct(FamilyRepository $familyRepository)
    {
        $this->familyRepository = $familyRepository;
    }

    public function load(ObjectManager $manager)
    {
        //? on instancie 2 membres fictifs dans un tableau 
        $members = [
            ["Omar","OmarSaleh@houmous.com"],
            ["Abdel","Abdel@kebab.fr"],
          ];

          $families = $this->familyRepository->findAll();

          //? pour chaque élément du tableau members_ , on prend chaque member_ 
          //? on leur set des elements par index de tableau                     
          foreach ($members as $memberInfos){
              $member = new Member();
              $member->setName($memberInfos[0]);
              $member->setEmail($memberInfos[1]);
              
              shuffle($families);     //? Random 
              for($i=0; $i<7; $i++){  //? Select two families 
                  $member->addFamily($families[$i]);
              }  

              //*! au moment où on va sauvegarder en db au va garder ceux persistés en mémoire 
              $manager->persist($member);
          }
          //*! tout les éléments persistés sont créés 
        $manager->flush();
    }

    //! en l'occurence -> Family 
    public function getDependencies()
    {
        return [
          FamilyFixtures::class
        ];
    }
}
